// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/dyd"

import (
	"bytes"
	"fmt"
	html0 "html"
	"io"
	"os"
	"strings"
	"time"

	"golang.org/x/net/html"
	"modernc.org/strutil"
)

var (
	// https://www.w3.org/TR/2011/WD-html-markup-20110113/syntax.html#syntax-elements
	voidHTMLElements = map[string]struct{}{
		"area":    {},
		"base":    {},
		"br":      {},
		"col":     {},
		"command": {},
		"embed":   {},
		"hr":      {},
		"img":     {},
		"input":   {},
		"keygen":  {},
		"link":    {},
		"meta":    {},
		"param":   {},
		"source":  {},
		"track":   {},
		"wbr":     {},
	}
)

func htmlFmt(path string) (err error) {
	const (
		backupRetries    = 5
		backupRetryDelay = 20 * time.Millisecond
		tagPI            = "p"
		tagPRE           = "e"
		tagStyle         = "s"
		tagText          = "t"
	)
	rawHTML, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	ptoks, err := tokenizeHTML(path, rawHTML)
	if err != nil {
		return err
	}

	if len(ptoks) == 0 || ptoks[len(ptoks)-1].Ch != tokEOF {
		return fmt.Errorf("%v: internal error (%v:)", path, origin(1))
	}

	synthHTML := bytes.NewBuffer(nil)
	style := 0
	pre := 0
	trimLeadingWhiteSpace := true
loop:
	for _, ptok := range ptoks {
		switch ptok.Ch {
		case tokHTML:
			sc := html.NewTokenizer(strings.NewReader(ptok.Src()))
		loop2:
			for {
				tokType := sc.Next()
				tok := sc.Token()
				switch tok.Type {
				case html.DoctypeToken:
					fmt.Fprintf(synthHTML, "%s", tok)
					trimLeadingWhiteSpace = false
				case html.TextToken:
					if trimLeadingWhiteSpace && strings.TrimSpace(tok.Data) == "" {
						break
					}

					switch {
					case style != 0:
						fmt.Fprintf(synthHTML, "\n%s%02x", tagStyle, tok.Data)
					case pre != 0:
						fmt.Fprintf(synthHTML, "\n%s%02x", tagPRE, tok.Data)
					default:
						fmt.Fprintf(synthHTML, "\n%s%02x", tagText, html0.EscapeString(tok.Data))
					}
					trimLeadingWhiteSpace = false
				case html.StartTagToken:
					fmt.Fprintf(synthHTML, "%s", tok)
					switch tok.Data {
					case "style":
						style++
					case "pre":
						pre++
					}
					trimLeadingWhiteSpace = false
				case html.EndTagToken:
					fmt.Fprintf(synthHTML, "%s", tok)
					switch tok.Data {
					case "style":
						style--
					case "pre":
						pre--
					}
					trimLeadingWhiteSpace = false
				case html.SelfClosingTagToken:
					fmt.Fprintf(synthHTML, "%s", tok)
					trimLeadingWhiteSpace = false
				case html.CommentToken:
					fmt.Fprintf(synthHTML, "%s", tok)
					trimLeadingWhiteSpace = false
				case html.ErrorToken:
					if err = sc.Err(); err != nil {
						if err != io.EOF {
							return err
						}
					}

					break loop2
				default:
					return fmt.Errorf("internal error: %v %v (%v:)", tokType, tok, origin(1))
				}
			}
		case tokPI:
			fmt.Fprintf(synthHTML, "\n%s%02x", tagPI, ptok.Src())
			trimLeadingWhiteSpace = false
		case tokEOF:
			break loop
		default:
			return fmt.Errorf("%v: internal error (%v:)", ptok.Position(), origin(1))
		}
	}

	// trc("==== synth ====\n%s----", synthHTML.Bytes())
	w0 := bytes.NewBuffer(nil)
	w := strutil.IndentFormatter(w0, "  ") // https://developers.google.com/style/html-formatting

	// defer func() { trc("==== w0 ====\n%s----", w0.Bytes()) }()

	var text []string

	emit0 := func(a []string) error {
		for _, v := range a {
			switch tag, s := v[:1], v[1:]; tag {
			case tagText:
				a := strings.Split(s, "\n")
				for i, v := range a {
					w.Format("%s", strings.TrimLeft(v, " \t"))
					if i != len(a)-1 {
						w.Format("\n")
					}
				}
			case tagPI:
				w.Format("%s", s)
			default:
				return fmt.Errorf("internal error: emit0 tag %q (%v:)", tag, origin(1))
			}
		}
		return nil
	}

	const (
		normal = iota
		preStartTag
		preEndTag
		final
	)
	disableTrim := 0
	emit := func(mode int) (err error) {
		defer func() { text = text[:0] }()

		if len(text) == 0 {
			return nil
		}

		if mode == final {
			for i := len(text) - 1; i >= 0; i-- {
				if strings.TrimSpace(text[i][1:]) == "" {
					text = text[:i]
					continue
				}

				break
			}
			if len(text) == 0 {
				w.Format("%s", "\n")
				return nil
			}

			mode = normal
		}

		if err = emit0(text[:len(text)-1]); err != nil {
			return err
		}

		s := text[len(text)-1]
		tag := s[:1]
		s = s[1:]
		a := strings.Split(s, "\n")
		switch tag {
		case tagText:
			switch mode {
			case normal:
				a := strings.Split(s, "\n")
				for i, v := range a {
					w.Format("%s", v)
					if i != len(a)-1 {
						w.Format("\n")
					}
				}
			case preStartTag, preEndTag:
				nl := false
				for i, v := range a {
					switch {
					case i == len(a)-1:
						switch strings.TrimSpace(v) {
						case "":
							// nop
						default:
							switch {
							case nl:
								w.Format("%s", strings.TrimLeft(v, " \t"))
							default:
								w.Format("%s", v)
							}
						}
					default:
						if disableTrim == 0 && nl {
							v = strings.TrimSpace(v)
						}
						w.Format("%s\n", v)
						nl = true
					}
				}
			default:
				return fmt.Errorf("internal error: emit last tag %q mode %v (%v:)", tag, mode, origin(1))
			}
		case tagStyle:
			for i, v := range a {
				switch {
				case i == len(a)-1:
					switch strings.TrimSpace(v) {
					case "":
						// nop
					default:
						w.Format("%s", strings.TrimLeft(v, " \t"))
					}
				case i == len(a)-2:
					w.Format("%s\n", v)
				default:
					w.Format("%s%s", v, "\n")
				}
			}
		case tagPI:
			w.Format("%s", s)
		case tagPRE:
			w.Format("%s", s)
		default:
			return fmt.Errorf("internal error: emit last tag %q (%v:)", tag, origin(1))
		}
		return nil
	}

	sc := html.NewTokenizer(synthHTML)
loop3:
	for {
		tokType := sc.Next()
		tok := sc.Token()
		switch tok.Type {
		case html.DoctypeToken:
			if err = emit(normal); err != nil {
				return err
			}

			w.Format("%s", tok)
		case html.TextToken:
			text = strings.Split(tok.Data, "\n")
			w := 0
			for _, v := range text {
				v = strings.TrimSpace(v)
				if v == "" {
					continue
				}

				text[w] = v[:1] + decodeHexString(v[1:])
				w++
			}
			text = text[:w]
		case html.StartTagToken:
			switch _, ok := voidHTMLElements[tok.Data]; {
			case ok:
				if err = emit(preStartTag); err != nil {
					return err
				}

				w.Format("%s", tok)
			default:
				if err = emit(preStartTag); err != nil {
					return err
				}

				w.Format("%s%i", tok)
			}
		case html.EndTagToken:
			if err = emit(preEndTag); err != nil {
				return err
			}

			w.Format("%u%s", tok)
		case html.SelfClosingTagToken, html.CommentToken:
			if err = emit(preStartTag); err != nil {
				return err
			}

			w.Format("%s", tok)
		case html.ErrorToken:
			if err = emit(final); err != nil {
				return err
			}

			if err = sc.Err(); err != nil {
				if err != io.EOF {
					return err
				}
			}

			break loop3
		default:
			return fmt.Errorf("internal error: %v %v (%v:)", tokType, tok, origin(1))
		}
	}

	for i := 0; i < backupRetries; i++ {
		nm := fmt.Sprintf("%s.%d", path, time.Now().UnixNano())
		f, err := os.OpenFile(nm, os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0660)
		if err != nil {
			time.Sleep(backupRetryDelay)
			continue
		}

		if _, err := f.Write(rawHTML); err != nil {
			return err
		}

		if err = f.Sync(); err != nil {
			return err
		}

		if err = f.Close(); err != nil {
			return err
		}

		// Final cleanup of right whitespace.
		nl := []byte("\n")
		a := bytes.Split(w0.Bytes(), nl)
		for i, v := range a {
			a[i] = bytes.TrimRight(v, " \t\r")
		}
		return os.WriteFile(path, bytes.Join(a, nl), 0666)
	}

	return fmt.Errorf("%s: cannot create backup", path)
}
