// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//TODO -tlsaddr -cert -pem
//TODO Support more targets via CGO SQLite

package main // import "modernc.org/dyd"

import (
	"bytes"
	"database/sql"
	"fmt"
	"go/scanner"
	"go/token"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"sort"
	"strings"

	"golang.org/x/mod/modfile"
	"golang.org/x/net/html"
	"modernc.org/gc/v3"
	"modernc.org/opt"
	mscanner "modernc.org/scanner"
	_ "modernc.org/sqlite"
)

const (
	defaultServeAddr = ":6226"
	serveFuncPrefix  = "Serve_"
)

func main() {
	if rc, err := main1(os.Args, os.Stdout, os.Stderr); err != nil || rc != 0 {
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		os.Exit(rc)
	}
}

func main1(osArgs []string, stdout, stderr io.Writer) (rc int, err error) {
	t := &task{
		optAddr:     defaultServeAddr,
		dydFile:     "dyd.go",
		osArgs:      osArgs,
		siteImports: map[string]struct{}{},
		siteMap:     map[string]*pageProducer{},
		urlInfos:    map[string]*urlInfo{},
	}
	set := opt.NewSet()
	set.Arg("addr", false, func(opt, arg string) error { t.optAddr = arg; return nil })
	set.Arg("fts", false, func(opt, arg string) error { t.optFTS = arg; return nil })
	set.Arg("goimports", false, func(opt, arg string) error { t.optGoimportsPath = arg; t.optGoimportsPathSet = true; return nil })
	set.Arg("modinit", false, func(opt, arg string) error { t.optModinit = arg; return nil })
	set.Opt("clean", func(opt string) error { t.optClean = true; return nil })
	set.Opt("htmlfmt", func(opt string) error { t.optHtmlfmt = true; return nil })
	set.Opt("serve", func(opt string) error { t.optServe = true; return nil })
	set.Opt("v", func(opt string) error { t.optVerbose = true; return nil })
	if err = set.Parse(osArgs[1:], func(arg string) error {
		if strings.HasPrefix(arg, "-") {
			return fmt.Errorf("unexpected option: %s", arg)
		}

		t.nonFlagArgs = append(t.nonFlagArgs, arg)
		return nil
	}); err != nil {
		return 2, err
	}

	if t.optGoimportsPath == "" && !t.optGoimportsPathSet {
		if t.optGoimportsPath, err = exec.LookPath("goimports"); err != nil {
			return 1, fmt.Errorf("%v\nuse -goimports <path> to use goimports at <path> or say -goimports='' to not use goimports", err)
		}
	}

	if err := t.main(); err != nil {
		return 1, err
	}

	return 0, nil
}

type pageProducer struct {
	funcName   string
	importPath string
	pkgName    string
}

type urlInfo struct {
	title string
	meta  [][]html.Attribute
}

type task struct {
	config            *gc.Config
	dydFile           string
	existingGoModPath string
	fts               *sql.DB
	mainFunc          *gc.FunctionDeclNode
	mainPkg           *gc.Package
	modulePath        string
	nonFlagArgs       []string
	optAddr           string
	optFTS            string
	optGoimportsPath  string
	optModinit        string
	osArgs            []string
	siteImports       map[string]struct{}
	siteMap           map[string]*pageProducer // rooted HTML path: producer
	urlInfos          map[string]*urlInfo
	wd                string

	optClean            bool
	optGoimportsPathSet bool
	optHtmlfmt          bool
	optServe            bool
	optVerbose          bool
}

func (t *task) main() (err error) {
	switch len(t.nonFlagArgs) {
	case 0:
		// ok
	case 1:
		if err := os.Chdir(t.nonFlagArgs[0]); err != nil {
			return fmt.Errorf("chdir: %v", err)
		}
	default:
		return fmt.Errorf("expected at most one non-flag argument: the webapp root path on disk: %v", t.nonFlagArgs)
	}

	if t.wd, err = os.Getwd(); err != nil {
		return fmt.Errorf("cannot determine working directory: %v", err)
	}

	if t.optClean {
		return t.clean()
	}

	t.existingGoModPath, err = findGoMod(t.wd)
	switch {
	case err != nil && t.optModinit != "":
		if err := t.exec("go", "mod", "init", t.optModinit); err != nil {
			return err
		}

		t.modulePath = t.optModinit
		t.existingGoModPath = filepath.Join(t.wd, "go.mod")
	case err != nil && t.modulePath == "":
		return err
	default:
		b, err := os.ReadFile(t.existingGoModPath)
		if err != nil {
			return err
		}

		if t.modulePath = modfile.ModulePath(b); t.modulePath == "" {
			return fmt.Errorf("%s: cannot determine module path", t.existingGoModPath)
		}
	}

	if t.config, err = gc.NewConfig(gc.ConfigLookup(
		func(dir, importPath, version string) (fsPath string, err error) {
			if !strings.Contains(importPath, ".") {
				return filepath.Join(runtime.GOROOT(), "src", importPath), nil
			}

			return dir, nil
		},
	)); err != nil {
		return err
	}

	if fn := t.optFTS; fn != "" {
		if err := os.Remove(fn); err != nil && !os.IsNotExist(err) {
			return err
		}

		t.fts, err = sql.Open("sqlite", fn)
		if err != nil {
			return err
		}

		defer t.fts.Close()

		if _, err := t.fts.Exec("create virtual table webindex using fts5(path, content);"); err != nil {
			return err
		}
	}

	if err = t.preprocess(); err != nil {
		return err
	}

	if err := t.buildSiteMap(); err != nil {
		return err
	}

	if t.optVerbose {
		var a []string
		for k := range t.siteMap {
			a = append(a, k)
		}
		sort.Strings(a)
		for _, k := range a {
			p := t.siteMap[k]
			fmt.Fprintf(os.Stderr, "url %v produced by %s.%s()\n", k, p.importPath, p.funcName)
		}
	}

	if t.mainPkg == nil {
		return fmt.Errorf("missing main package in web root at %s", t.wd)
	}

	if err := t.generateDyd(); err != nil {
		return err
	}

	mainFile := filepath.Join(t.wd, "main.go")
	if t.mainFunc != nil {
		mainFile = filepath.Join(t.wd, filepath.Base(t.mainFunc.Position().Filename))
	}

	switch err := t.verifySafeToWrite(mainFile); {
	case err == nil:
		if err := t.generateMain(mainFile); err != nil {
			return err
		}
	}

	if err := t.exec("go", "mod", "tidy"); err != nil {
		return err
	}

	if !t.optServe {
		return nil
	}

	fmt.Fprintf(os.Stderr, "starting server at %s\n", t.optAddr)
	return t.exec("go", "run", ".")
}

func (t *task) exec(prog string, args ...string) (err error) {
	if t.optVerbose {
		fmt.Fprintf(os.Stderr, "executing %v\n", append([]string{prog}, args...))
	}
	if out, err := exec.Command(prog, args...).CombinedOutput(); err != nil {
		return fmt.Errorf("executing %v:\n%s\nFAIL: %s", append([]string{prog}, args...), out, err)
	}

	return nil
}

func (t *task) clean() (err error) {
	if err := filepath.WalkDir(t.wd, func(path string, d fs.DirEntry, errIn error) (errOut error) {
		if errIn != nil {
			return errIn
		}

		if d.IsDir() || filepath.Ext(path) != ".go" {
			return nil
		}

		generated, errIn := t.verifySafeToWrite0(path)
		if generated && errIn == nil {
			if t.optVerbose {
				fmt.Fprintf(os.Stderr, "removing %s\n", path)
			}
			return os.Remove(path)
		}

		return nil
	}); err != nil {
		return err
	}

	// $ go clean eats index.html: https://go.dev/play/p/KPGjk3KErwL
	//
	// To avoid this from happening, we must call go clean only after removing the
	// generated files.

	return t.exec("go", "clean")
}

func (t *task) generateMain(fn string) (err error) {
	if t.optVerbose {
		fmt.Fprintf(os.Stderr, "generating %s\n", fn)
	}

	b := bytes.NewBuffer(nil)

	defer func() {
		if err != nil {
			return
		}

		if err = os.WriteFile(fn, b.Bytes(), 0660); err != nil {
			return
		}

		if t.optGoimportsPath == "" {
			return
		}

		err = t.exec(t.optGoimportsPath, "-l", "-w", fn)
	}()

	fmt.Fprintf(b, `// Code generated by %v, DO NOT EDIT.

package main

func main() {
	log.Fatal(dydStart())
}`,
		t.osArgs[0],
	)
	return nil
}

func (t *task) generateDyd() (err error) {
	if t.optVerbose {
		fmt.Fprintf(os.Stderr, "generating %s\n", t.dydFile)
	}

	if err = t.verifySafeToWrite(t.dydFile); err != nil {
		return err
	}

	b := bytes.NewBuffer(nil)

	defer func() {
		if err != nil {
			return
		}

		if err = os.WriteFile(t.dydFile, b.Bytes(), 0660); err != nil {
			return
		}

		if t.optGoimportsPath == "" {
			return
		}

		err = t.exec(t.optGoimportsPath, "-l", "-w", t.dydFile)
	}()

	fmt.Fprintf(b, `// Code generated by %v, DO NOT EDIT.

package main

import (
	"net/http"

	"modernc.org/dyd/dyd"`,
		t.osArgs[0],
	)
	var a []string
	for k := range t.siteImports {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, k := range a {
		fmt.Fprintf(b, "\n%q", k)
	}
	fmt.Fprintf(b, `
)

// dydStart starts the webapp. dydStart always returns a non-nil error.
func dydStart(beforeListen ...func(a dyd.App) error) error {`,
	)

	defer b.WriteString("\n}\n")

	fmt.Fprintf(b, `
app, err := dyd.NewApp()
if err != nil {
	return err
}
`)

	a = a[:0]
	for k := range t.siteMap {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, url := range a {
		p := t.siteMap[url]
		var qualifier string
		if p.pkgName != "main" {
			qualifier = fmt.Sprintf("%s.", filepath.Base(p.importPath))
		}
		fmt.Fprintf(b, "\napp.Bind(%q, %s%s)", url, qualifier, p.funcName)
		n := t.urlInfos[url]
		if n == nil {
			continue
		}

		for _, v := range n.meta {
			fmt.Fprintf(b, "\napp.AddMeta(%q, []html.Attribute{", url)
			for _, a := range v {
				fmt.Fprintf(b, "\n%#v,", a)
			}
			fmt.Fprintf(b, "\n})")
		}
		if s := n.title; s != "" {
			fmt.Fprintf(b, "\napp.AddTitle(%q, %q)", url, s)
		}
	}

	fmt.Fprintf(b, "\nfor _, f := range beforeListen { if err = f(app); err != nil { return err }}")
	fmt.Fprintf(b, "\nreturn app.ListenAndServe(%q)", t.optAddr)
	return nil
}

func (t *task) buildSiteMap() error {
	if t.optVerbose {
		fmt.Fprint(os.Stderr, "building site map\n")
	}
	return filepath.WalkDir(t.wd, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			m, err := filepath.Glob(filepath.Join(path, "*.go"))
			if err != nil {
				return nil
			}

			if len(m) != 0 {
				return t.collectPageProducers(path)
			}
		}

		return nil
	})
}

func (t *task) collectPageProducers(dir string) (err error) {
	repoRoot, _ := filepath.Split(t.existingGoModPath)
	rel, err := filepath.Rel(repoRoot, dir)
	if err != nil {
		return fmt.Errorf("determining relative path (%s, %s): %v", repoRoot, dir, err)
	}

	webRel, err := filepath.Rel(t.wd, dir)
	if err != nil {
		return fmt.Errorf("determining relative path (%s, %s): %v", t.wd, dir, err)
	}

	pkgDir := filepath.Join(repoRoot, rel)
	pkgImportPath := path.Join(t.modulePath, rel)
	pkg, err := t.config.NewPackage(pkgDir, pkgImportPath, "", nil, false, gc.TypeCheckNone)
	if err != nil {
		return err
	}

	var pkgName string
	for _, v := range pkg.AST {
		pkgName = v.SourceFile.PackageClause.PackageName.Src()
		break
	}
	isMain := false
	if pkgName == "main" {
		if dir != t.wd {
			return nil
		}

		isMain = true
		t.mainPkg = pkg
	}
	pkg.Scope.Iterate(func(name string, n gc.Node) (stop bool) {
		switch x := n.(type) {
		case *gc.FunctionDeclNode:
			if isMain && name == "main" {
				t.mainFunc = x
				return false
			}

			if !strings.HasPrefix(name, serveFuncPrefix) {
				return false
			}

			if err = t.verifyProducerSignature(x.Signature); err != nil {
				return true
			}

			produces := decodeFileName(name[len(serveFuncPrefix):])
			webPath := path.Join("/", webRel, produces)
			t.siteMap[webPath] = &pageProducer{
				pkgName:    pkgName,
				funcName:   name,
				importPath: pkgImportPath,
			}
			if !isMain {
				t.siteImports[pkgImportPath] = struct{}{}
			}
		}
		return false
	})
	return err
}

func (t *task) verifyProducerSignature(n *gc.SignatureNode) error {
	ord := 0
	for l := n.Parameters.ParameterDeclList; l != nil; l = l.List {
		pd := l.ParameterDecl
		switch ord {
		case 0:
			// ctx *dyd.Context
			if pd.IdentifierList.Len() > 1 ||
				strings.TrimSpace(pd.TypeNode.Source(false)) != "*dyd.Context" {
				return fmt.Errorf("%v: incompatible signature", pd.Position())
			}
		case 1:
			if pd.IdentifierList.Len() > 1 || strings.TrimSpace(pd.TypeNode.Source(false)) != "func(s string, args ...any) error" {
				return fmt.Errorf("%v: incompatible signature", pd.Position())
			}
		default:
			return fmt.Errorf("%v: too many parameters", l.ParameterDecl.Position())
		}
		ord++
	}
	if ord != 2 {
		return fmt.Errorf("%v: not enough parameters", n.Position())
	}

	return nil
}

func (t *task) preprocess() error {
	if t.optVerbose {
		fmt.Fprint(os.Stderr, "preprocessing\n")
	}
	return filepath.WalkDir(t.wd, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		switch strings.ToLower(filepath.Ext(path)) {
		case ".html":
			return t.preprocessHTML(path)
		}
		return nil
	})
}

func (t *task) preprocessHTML(path string) (err error) {
	if t.optHtmlfmt {
		if err = htmlFmt(path); err != nil {
			return err
		}
	}

	b, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	toks, err := tokenizeHTML(path, b)
	if err != nil {
		return err
	}

	if len(toks) == 0 || toks[len(toks)-1].Ch != tokEOF {
		return fmt.Errorf("%v: internal error (%v:)", path, origin(1))
	}

	toks = t.normalizeTokens(toks)
	if len(toks) == 1 {
		return fmt.Errorf("%v: file contains no non-blank tokns", toks[0].Position())
	}

	targetFN := path + ".go"
	if err := t.verifySafeToWrite(targetFN); err != nil {
		return err
	}

	switch tok := toks[0]; tok.Ch {
	case tokHTML, tokEOF, tokPI:
		return t.generateProducer(path, targetFN, toks)
	default:
		return fmt.Errorf("%v: internal error (%v:)", tok.Position(), origin(1))
	}
}

func (t *task) generateProducer(inputFN, targetFN string, toks []mscanner.Token) (err error) {
	rel, err := filepath.Rel(t.wd, inputFN)
	if err != nil {
		return fmt.Errorf("determining relative path (%s, %s): %v", t.wd, inputFN, err)
	}

	b := bytes.NewBuffer(nil)

	defer func() {
		if err != nil {
			return
		}

		if err = os.WriteFile(targetFN, b.Bytes(), 0660); err != nil {
			return
		}

		if t.optGoimportsPath == "" {
			return
		}

		err = t.exec(t.optGoimportsPath, "-l", "-w", targetFN)
	}()

	url := path.Join("/", filepath.ToSlash(rel))
	if err := t.index(url, toks); err != nil {
		return err
	}

	t.urlInfos[url] = &urlInfo{}

	fmt.Fprintf(b, `// Code generated by %v, DO NOT EDIT.

`, t.osArgs[0])
	abs, err := filepath.Abs(inputFN)
	if err != nil {
		return err
	}

	dir, _ := filepath.Split(abs)
	pkgName := filepath.Base(dir)
	if filepath.Clean(dir) == t.wd {
		pkgName = "main"
	}
	funcName := serveFuncPrefix + encodeFileName(filepath.Base(inputFN))
	fmt.Fprintf(b, `package %s

import (
	"database/sql"

	"modernc.org/dyd/dyd"
)

// %s produces %s.
func %[2]s(ctx *dyd.Context, write func(s string, args ...any) error) {`,
		pkgName,
		funcName,
		url,
	)

	defer b.WriteString("\n}\n")
	htmlSrc := bytes.NewBuffer(nil)
	for _, tok := range toks {
		switch tok.Ch {
		case tokHTML:
			src := tok.Src()
			htmlSrc.WriteString(src)
			fmt.Fprintf(b, "\nwrite(%s)", niceHTMLLiteral(src))
		case tokPI:
			b.WriteByte('\n')
			s := tok.Src()
			b.WriteString(s[len("<?") : len(s)-len("?>")])
		case tokEOF:
			return t.processHTML(inputFN, url, htmlSrc.Bytes())
		default:
			return fmt.Errorf("%v: internal error (%v:)", tok.Position(), origin(1))
		}
	}
	return fmt.Errorf("%v: internal error (%v:)", inputFN, origin(1))
}

func hasAttr(tok html.Token, s string) bool {
	for _, v := range tok.Attr {
		if strings.EqualFold(v.Key, s) {
			return true
		}
	}
	return false
}

func (t *task) index(url string, ptoks []mscanner.Token) (err error) {
	if t.fts == nil {
		return nil
	}

	var content []string
	var noindex int
	var stack []int
	var seenDoctype, seenHTMLStart, seenHTMLEnd bool
	for _, ptok := range ptoks {
		switch ptok.Ch {
		case tokHTML:
			sc := html.NewTokenizer(strings.NewReader(ptok.Src()))
		loop:
			for {
				sc.Next()
				tok := sc.Token()
				switch tok.Type {
				case html.DoctypeToken:
					seenDoctype = true
				case html.TextToken:
					if noindex > 0 {
						break
					}

					content = append(content, tok.Data)
				case html.StartTagToken:
					switch tok.Data {
					case "html":
						seenHTMLStart = true
					}
					if _, ok := voidHTMLElements[tok.Data]; ok {
						break
					}

					stack = append(stack, noindex)
					if hasAttr(tok, "noindex") {
						noindex++
					}
				case html.EndTagToken:
					switch tok.Data {
					case "html":
						seenHTMLEnd = true
					}
					n := len(stack)
					if n == 0 {
						break
					}

					noindex = stack[n-1]
					stack = stack[:n-1]
				case html.ErrorToken:
					if err = sc.Err(); err != nil {
						if err != io.EOF {
							return err
						}
					}

					break loop
				}
			}
		}
	}
	if len(content) == 0 || !seenDoctype || !seenHTMLStart || !seenHTMLEnd {
		return nil
	}

	_, err = t.fts.Exec("insert into webindex values(?, ?);", url, strings.Join(content, " "))
	return err
}

func (t *task) processHTML(inputFN, url string, src []byte) (err error) {
	doc, err := html.Parse(bytes.NewReader(src))
	if err != nil {
		return err
	}

	urlInfo := t.urlInfos[url]
	var walk func(n *html.Node)
	walk = func(n *html.Node) {
		for ; n != nil; n = n.NextSibling {
			if n.FirstChild != nil {
				walk(n.FirstChild)
			}
			switch n.Type {
			case html.ElementNode:
				switch n.Data {
				case "meta":
					urlInfo.meta = append(urlInfo.meta, n.Attr)
				}
			case html.TextNode:
				if n.Parent.Type == html.ElementNode && n.Parent.Data == "title" {
					t.urlInfos[url].title = n.Data
				}
			}
		}
	}

	walk(doc)
	return nil
}

func (t *task) normalizeTokens(toks []mscanner.Token) (r []mscanner.Token) {
	eof := toks[len(toks)-1]
	toks = toks[:len(toks)-1]
	for i, tok := range toks {
		if strings.TrimSpace(tok.Src()) != "" {
			toks = toks[i:]
			break
		}
	}
	for i := len(toks) - 1; i > 0; i-- {
		tok := toks[i]
		if s := strings.TrimSpace(tok.Src()); s != "" {
			toks = toks[:i+1]
			break
		}
	}
	return append(toks, eof)
}

func (t *task) verifySafeToWrite(path string) (err error) {
	_, err = t.verifySafeToWrite0(path)
	return err
}

func (t *task) verifySafeToWrite0(path string) (generated bool, err error) {
	fi, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
	}

	if fi.IsDir() {
		return false, fmt.Errorf("cannot write output file, existing %s is a directory", path)
	}

	b, err := os.ReadFile(path)
	if err != nil {
		return false, fmt.Errorf("cannot write output file, reading existing %s: %v", path, err)
	}

	var s scanner.Scanner
	fs := token.NewFileSet()
	s.Init(fs.AddFile(path, -1, len(b)), b, nil, scanner.ScanComments)
	for {
		switch _, tok, lit := s.Scan(); tok {
		case token.COMMENT:
			// https://pkg.go.dev/cmd/go#hdr-Generate_Go_files_by_processing_source
			if !strings.Contains(lit, "\n") && strings.HasPrefix(lit, "// Code generated ") && strings.HasSuffix(lit, " DO NOT EDIT.") {
				return true, nil
			}
		case token.EOF:
			return false, nil
		default:
			return false, fmt.Errorf("refusing to overwrite %s: `^// Code generated .* DO NOT EDIT\\.$` marker not detected", path)
		}
	}
}
