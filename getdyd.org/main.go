// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command getdyd.org is the web server for www.getdyd.org.
package main

import (
	"embed"
	"flag"
	"net/http"
	"os"
	"os/signal"

	"github.com/golang/glog"
)

var (
	//go:embed assets
	assets embed.FS
)

func main() {
	defer func() {
		glog.Info("exiting")
		glog.Flush()
	}()

	flag.Parse()

	go func() {
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, os.Interrupt)
		sig := <-ch
		glog.Infof("exiting on %s\n", sig)
		glog.Flush()
		os.Exit(0)
	}()

	glog.Infof("started %v", os.Args)
	http.Handle("/assets/", http.FileServer(http.FS(assets)))
	glog.Error(dydStart())
}
