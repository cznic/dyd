# Copyright 2023 The dyd Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all clean edit editor examples test work

all:

clean:
	rm -f *.text *.out *.test
	dyd -clean getdyd.org
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile go.mod builder.json *.go & fi

editor:
	date > log-editor
	gofmt -l -s -w *.go
	go test -failfast . ./dyd 2>&1 | tee -a log-editor
	go install 2>&1 2>&1 | tee -a log-editor
	dyd -htmlfmt getdyd.org 2>&1 | tee -a log-editor
	# dyd -htmlfmt _examples/helloworld 2>&1 | tee -a log-editor
	gofmt -l -s -w . 2>&1 | tee -a log-editor
	go build -v -o /dev/null . ./dyd ./getdyd.org 2>&1 | tee -a log-editor
	golint 2>&1 | tee -a log-editor
	staticcheck 2>&1 | tee -a log-editor
	date >> log-editor
	find -name \*.html.[0-9]\*  -exec mv {} /tmp \;

examples:
	./_examples/examples.sh
	find -name \*.html.[0-9]\*  -exec mv {} /tmp \;

test:
	date > log-test
	go test -failfast . ./dyd 2>&1 | tee -a log-test
	go build -v -o /dev/null . ./dyd 2>&1 | tee -a log-test
	date >> log-test

work:
	rm -f go.work
	go work init
	go work use . ../gc/v3
