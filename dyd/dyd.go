// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package dyd provides runtime support for web servers created by the dyd
// command.
package dyd // import "modernc.org/dyd/dyd"

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	"net/http"
	"path"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/net/html"
	_ "modernc.org/sqlite"
)

var (
	//go:embed w3.css
	w3css string
)

const (
	dbName         = "dyd.sqlite?_time_format=sqlite&_pragma=journal_mode(wal)"
	kvName         = "dyd_kv"
	recursionLimit = 1000
)

var (
	_ App = (*app)(nil)
)

// PreRequestHandler handles user defined actions before looking for a
// RequestHandler using ctx.URL.Path.  If the function return a non nil error
// no request handler will be run.
type PreRequestHandler func(ctx *Context) error

// RequestHandler handles a http request, or a part of if, using 'ctx' and 'write'.
type RequestHandler func(ctx *Context, write func(s string, args ...any) error)

// App represents a dyd web app.
type App interface {
	AddMeta(url string, attr []html.Attribute) error
	AddPreRequestHandler(handler PreRequestHandler) (id int)
	AddTitle(url string, title string) error
	Bind(url string, handler RequestHandler)
	DB() (*sql.DB, error)
	HasHandler(url string) bool
	KV() *KV
	ListenAndServe(addr string) error
}

// URLInfo provides information about a server page.
type URLInfo struct {
	handler RequestHandler
	meta    [][]html.Attribute
	path    string // url used to register the handler
	title   string
}

// Meta returns html.Attributes of <meta> elements.
func (n *URLInfo) Meta() [][]html.Attribute { return n.meta }

// Title returns the text of the <title> element, if any.
func (n *URLInfo) Title() string { return n.title }

type app struct {
	sync.Mutex
	db0                *sql.DB
	db0Error           error
	dbOnce             sync.Once
	kv                 *KV
	reqSeq             atomic.Int64
	rootHandler        RequestHandler
	urlRegister        map[string]*URLInfo
	preRequestHandlers []PreRequestHandler
}

func newApp() (*app, error) {
	r := &app{
		urlRegister: map[string]*URLInfo{},
	}
	r.kv = newKV(r, kvName)
	return r, nil
}

// NewApp returns a newly created App or an error, if any.
func NewApp() (App, error) {
	return newApp()
}

func (a *app) KV() *KV { return a.kv }

func (a *app) HasHandler(url string) bool {
	a.Lock()

	defer a.Unlock()

	info := a.urlRegister[url]
	return info != nil && info.handler != nil
}

func (a *app) AddPreRequestHandler(handler PreRequestHandler) (id int) {
	a.Lock()

	defer a.Unlock()

	id = len(a.preRequestHandlers)
	a.preRequestHandlers = append(a.preRequestHandlers, handler)
	return id
}

func (a *app) AddTitle(url string, title string) error {
	a.Lock()

	defer a.Unlock()

	info := a.urlRegister[url]
	if info == nil {
		return fmt.Errorf("AddTitke: url not registered: %s", url)
	}

	info.title = title
	return nil
}

func (a *app) AddMeta(url string, attrs []html.Attribute) error {
	a.Lock()

	defer a.Unlock()

	info := a.urlRegister[url]
	if info == nil {
		return fmt.Errorf("AddMeta: url not registered: %s", url)
	}

	info.meta = append(info.meta, attrs)
	return nil
}

func (a *app) Bind(url string, handler RequestHandler) {
	a.Lock()

	defer a.Unlock()

	a.bind(url, handler)
}

func (a *app) runPreRequestHandlers(ctx *Context) (err error) {
	a.Lock()
	handlers := append([]PreRequestHandler(nil), a.preRequestHandlers...)
	a.Unlock()

	for _, handler := range handlers {
		if err = handler(ctx); err != nil {
			return err
		}
	}
	return nil
}

func (a *app) bind(url string, handler RequestHandler) {
	if _, ok := a.urlRegister[url]; ok {
		panic(fmt.Errorf("internal error, url already bound: %s", url))
	}

	urlInfo := &URLInfo{
		handler: handler,
		path:    url,
	}
	a.urlRegister[url] = urlInfo
	_, file := path.Split(url)
	// HTML files named `.*_\.html`, like top_.html, are not externally bound.
	if strings.HasSuffix(file, "_.html") {
		return
	}

	http.HandleFunc(url, func(rw http.ResponseWriter, rq *http.Request) {
		ctx := &Context{
			ReqContext:     context.Background(),
			ReqSequence:    a.reqSeq.Add(1),
			Request:        rq,
			ResponseWriter: rw,
			URLInfo:        a.urlRegister[rq.URL.Path],
			app:            a,
			kv:             a.kv,
		}

		defer ctx.close()

		if err := a.runPreRequestHandlers(ctx); err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		if ctx.URLInfo = a.urlRegister[rq.URL.Path]; ctx.URLInfo == nil {
			http.NotFound(rw, rq)
			return
		}

		ctx.URLInfo.handler(ctx, ctx.write)
	})
	if url == "/index.html" {
		a.rootHandler = handler
		a.bind("/", handler)
		a.urlRegister["/"] = urlInfo
	}
}

func (a *app) DB() (*sql.DB, error) {
	a.dbOnce.Do(func() {
		a.db0, a.db0Error = sql.Open("sqlite", dbName)
	})
	return a.db0, a.db0Error
}

func (a *app) ListenAndServe(addr string) error { return http.ListenAndServe(addr, nil) }

// Context provides access to the state of the server/session/request/database
// etc. Every web request creates a new Context instance which is automatically
// destroyed when the outermost serving function returns.
//
// Context is not safe for concurrent use by multiple goroutines.
type Context struct {
	IncludeArgs    []any
	ReqContext     context.Context
	ReqSequence    int64
	Request        *http.Request
	ResponseWriter http.ResponseWriter
	URLInfo        *URLInfo
	app            *app
	cancelFuncs    []context.CancelFunc
	conns          []*sql.Conn
	kv             *KV
	url            string // reflects includes

	recursionLevel int
}

func (c *Context) close() (err error) {
	defer func() { *c = Context{} }()

	for i, v := range c.cancelFuncs {
		v()
		c.cancelFuncs[i] = nil
	}

	for i, v := range c.conns {
		if e := v.Close(); e != nil && err == nil {
			err = e
		}
		c.conns[i] = nil
	}
	return err
}

// URLInfoFor returns the URLInfo for 'url' or nil, if 'url' is not registered.
func (c *Context) URLInfoFor(url string) *URLInfo { return c.app.urlRegister[url] }

// WithCancel returns context.WithCancel(parent). The cancel function is run
// automatically when 'c' is destroyed.
func (c *Context) WithCancel(parent context.Context) context.Context {
	if parent == nil {
		parent = context.Background()
	}
	r, f := context.WithCancel(parent)
	c.cancelFuncs = append(c.cancelFuncs, f)
	return r
}

// WithDeadline returns context.WithDeadline(parent, d). The cancel function is
// run automatically when 'c' is destroyed.
func (c *Context) WithDeadline(parent context.Context, d time.Time) context.Context {
	if parent == nil {
		parent = context.Background()
	}
	r, f := context.WithDeadline(parent, d)
	c.cancelFuncs = append(c.cancelFuncs, f)
	return r
}

// WithTimeout returns context.WithTimeout(parent, timeout). The cancel
// function is run automatically when 'c' is destroyed.
func (c *Context) WithTimeout(parent context.Context, timeout time.Duration) context.Context {
	if parent == nil {
		parent = context.Background()
	}
	r, f := context.WithTimeout(parent, timeout)
	c.cancelFuncs = append(c.cancelFuncs, f)
	return r
}

// KV returns the key value store associated with the server instance or an
// error, if any.
func (c *Context) KV() *KV { return c.kv }

// DBConn returns a new *sql.Conn or an error, if any. All connections are
// automatically closed when the serving function returns.
func (c *Context) DBConn(ctx context.Context) (*sql.Conn, error) {
	db, err := c.app.DB()
	if err != nil {
		return nil, err
	}

	if ctx == nil {
		ctx = context.Background()
	}
	conn, err := db.Conn(ctx)
	if err != nil {
		return nil, err
	}

	c.conns = append(c.conns, conn)
	return conn, nil
}

// Include will call the serving function bound to url, if any. For example
//
//	ctx.Include("/path/to/file.html")
func (c *Context) Include(url string, args ...any) (err error) {
	restorePath := c.Request.URL.Path
	restoreArgs := c.IncludeArgs
	defer func() {
		c.Request.URL.Path = restorePath
		c.IncludeArgs = restoreArgs
	}()

	switch {
	case strings.HasPrefix(url, "."):
		// resolve relative paths
		dir, _ := path.Split(c.url)
		url = path.Join(dir, url)
	case !strings.HasPrefix(url, "/"):
		// canonical paths are rooted
		url = path.Join("/", url)
	}

	c.Request.URL.Path = url
	if err = c.app.runPreRequestHandlers(c); err != nil {
		return err
	}

	c.recursionLevel++
	if c.recursionLevel > recursionLimit {
		panic(fmt.Errorf("include recursion limit reached"))
	}

	defer func() { c.recursionLevel-- }()

	c.url = c.Request.URL.Path
	if info := c.app.urlRegister[c.url]; info != nil {
		c.Request.URL.Path = restorePath
		c.IncludeArgs = args
		info.handler(c, c.write)
		return nil
	}

	return fmt.Errorf("no serving function registered for url: %s", url)
}

// W3CSS returns the content of
//
//	https://www.w3schools.com/w3css/4/w3.css
//
// without using an internet connection. The latest version at the above URL
// is, as of 2023-05-23
//
//	/* W3.CSS 4.15 December 2020 by Jan Egil and Borge Refsnes */
//
// It can be used like
//
//	<!DOCTYPE html>
//	<html>
//	  <head>
//	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
//	  <style><? write("%s", ctx.W3CSS()) ?></style>
//	  </head>
//	  <body>
//	  </body>
//	</html>
func (c *Context) W3CSS() string { return w3css }

func (c *Context) write(s string, args ...any) error {
	_, err := fmt.Fprintf(c.ResponseWriter, s, args...)
	return err
}

// KV models a simple key/value store backed by a SQLite table with this schema
//
//	create table <table name> (key text primary key on conflict replace, value);
//
// Note the 'value' column has no type a thus no affinity. Quoting
// https://www.sqlite.org/datatype3.html#datetime:
//
//	SQLite does not have a storage class set aside for storing dates and/or
//	times. Instead, the built-in Date And Time Functions of SQLite are capable
//	of storing dates and times as TEXT, REAL, or INTEGER values: ...
//
// This means KV.Get(key, &dest) where dest is *time.Time or *sql.NullTime will
// _not_ work. The proper destinations of Get reflect the SQLite only data
// types listed at https://www.sqlite.org/datatype3.html#affinity:
//
//   - TEXT
//   - NUMERIC
//   - INTEGER
//   - REAL
//   - BLOB
//
// To read time-representing values, use the GetTime method.
type KV struct {
	mu sync.Mutex

	a          *app
	conn0      *sql.Conn
	delete     *sql.Stmt
	deleteLike *sql.Stmt
	err0       error
	get        *sql.Stmt
	like       *sql.Stmt
	once       sync.Once
	set        *sql.Stmt
	tableName  string
}

func newKV(a *app, tableName string) *KV {
	return &KV{
		a:         a,
		tableName: tableName,
	}
}

func (s *KV) boot() (err error) {
	s.once.Do(func() {
		ctx := context.Background()
		var db *sql.DB
		if db, s.err0 = s.a.DB(); s.err0 != nil {
			return
		}

		if s.conn0, s.err0 = db.Conn(ctx); err != nil {
			return
		}

		if _, s.err0 = s.conn0.ExecContext(ctx, fmt.Sprintf("create table if not exists %s(key text primary key on conflict replace, value);", s.tableName)); s.err0 != nil {
			return
		}

		if s.set, s.err0 = s.conn0.PrepareContext(ctx, fmt.Sprintf("insert into %s values(?, ?)", s.tableName)); s.err0 != nil {
			return
		}

		if s.get, s.err0 = s.conn0.PrepareContext(ctx, fmt.Sprintf("select value from %s where key = ?", s.tableName)); s.err0 != nil {
			return
		}

		if s.like, s.err0 = s.conn0.PrepareContext(ctx, fmt.Sprintf("select key, value from %s where key like ? order by key", s.tableName)); s.err0 != nil {
			return
		}

		if s.delete, s.err0 = s.conn0.PrepareContext(ctx, fmt.Sprintf("delete from %s where key = ?", s.tableName)); s.err0 != nil {
			return
		}

		if s.deleteLike, s.err0 = s.conn0.PrepareContext(ctx, fmt.Sprintf("delete from %s where key like ?", s.tableName)); s.err0 != nil {
			return
		}

	})
	return s.err0
}

// Delete removes the value associated with key. The method is safe for
// concurrent use by multiple goroutines, all operations are serialized.
// Meaning all other operations on 's' are blocked until Delete completes.
func (s *KV) Delete(ctx context.Context, key string) (err error) {
	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	_, err = s.delete.ExecContext(ctx, key)
	return err
}

// DeleteLike removes all value associated with keys matching 'pattern'  using
// the SQLite operator LIKE (https://www.sqlite.org/lang_expr.html#like). The
// method is safe for concurrent use by multiple goroutines, all operations are
// serialized.  Meaning all other operations on 's' are blocked until Delete
// completes.
func (s *KV) DeleteLike(ctx context.Context, pattern string) (err error) {
	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	_, err = s.deleteLike.ExecContext(ctx, pattern)
	return err
}

// Set sets the value associated with key. The method is safe for concurrent
// use by multiple goroutines, all operations are serialized. Meaning all other
// operations on 's' are blocked until Set completes.
func (s *KV) Set(ctx context.Context, key string, value any) (err error) {
	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	_, err = s.set.ExecContext(ctx, key, value)
	return err
}

// KVPair represent a Value associated with Key.
type KVPair struct {
	Key   string
	Value any
}

// BatchSet sets the values associated with keys. The method is safe for concurrent
// use by multiple goroutines, all operations are serialized. Meaning all other
// operations on 's' are blocked until BatchSet completes.
//
// BatchSet atomically sets all pairs or none of them.
func (s *KV) BatchSet(ctx context.Context, pairs []KVPair) (err error) {
	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	tx, err := s.conn0.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	set, err := tx.PrepareContext(ctx, fmt.Sprintf("insert into %s values(?, ?)", s.tableName))
	if err != nil {
		return err
	}

	for _, pair := range pairs {
		if _, err = set.ExecContext(ctx, pair.Key, pair.Value); err != nil {
			return err
		}
	}
	return err
}

// Get retrieves the value associated with key, if any. The method is safe for
// concurrent use by multiple goroutines, all operations are fully
// isolated/serialized. Meaning all other operations on 's' are blocked until
// Get completes.
//
// To detect null values, pass an instance of one of the sql.Null* types or any
// other type implementing the sql.Scan interface{}. More info about acceptable
// 'dest' types available at https://pkg.go.dev/database/sql#Rows.Scan.
func (s *KV) Get(ctx context.Context, key string, dest any) (err error) {
	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	return s.get.QueryRowContext(ctx, key).Scan(dest)
}

// GetTime is the is the same as Get, but expects the produced value to be NULL
// or to be a string representation of a datetime value. The 'dest' should have
// type *any, *time.Time or *sql.NullTime.
func (s *KV) GetTime(ctx context.Context, key string, dest any) (err error) {
	const format = "2006-01-02 15:04:05.999999999-07:00"

	if err = s.boot(); err != nil {
		return err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	var v any
	if err = s.get.QueryRowContext(ctx, key).Scan(&v); err != nil {
		return err
	}

	var result time.Time
	var valid bool
	switch x := v.(type) {
	case nil:
		// ok
	case string:
		result, err = time.Parse(format, x)
		if err != nil {
			return err
		}

		valid = true
	default:
		return fmt.Errorf("%T.GetTime, unexpected value type %T", s, v)
	}
	switch y := dest.(type) {
	case *sql.NullTime:
		y.Valid = valid
		y.Time = result
	case *time.Time:
		*y = result
	case *any:
		*y = result
	default:
		return fmt.Errorf("%T.GetTime, unexpected destination type %T", s, dest)
	}
	return nil
}

// GetLike retrieves all values associated with keys matching 'pattern' using
// the SQLite operator LIKE (https://www.sqlite.org/lang_expr.html#like).  The
// method is safe for concurrent use by multiple goroutines, all operations are
// fully isolated/serialized. Meaning all other operation on 's' are blocked
// until GetLike completes.
//
// When GetLike returns an error, it still returns also any pairs already produced.
//
// Returning no pairs is not an error.
func (s *KV) GetLike(ctx context.Context, pattern string) (pairs []KVPair, err error) {
	if err = s.boot(); err != nil {
		return nil, err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	rows, err := s.like.QueryContext(ctx, pattern)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var pair KVPair
		if err = rows.Scan(&pair.Key, &pair.Value); err != nil {
			return pairs, err
		}

		pairs = append(pairs, pair)
	}

	return pairs, rows.Err()
}

// Inc atomically increments an int64 value at key. If no value for the key
// exists or if the value is NULL it is considered to be zero. Inc returns the
// new value or an error, if any.
func (s *KV) Inc(ctx context.Context, key string) (r int64, err error) {
	if err = s.boot(); err != nil {
		return 0, err
	}

	s.mu.Lock()

	defer s.mu.Unlock()

	if ctx == nil {
		ctx = context.Background()
	}
	var dest sql.NullInt64
	if err := s.get.QueryRowContext(ctx, key).Scan(&dest); err != nil && err != sql.ErrNoRows {
		return 0, err
	}

	if !dest.Valid {
		dest.Int64 = 0
	}
	dest.Int64++
	_, err = s.set.ExecContext(ctx, key, dest.Int64)
	return dest.Int64, err
}
