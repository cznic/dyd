// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the GO-LICENSE file.

package dyd // import "modernc.org/dyd/dyd"

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

// testing.TempDir seems to have some issues on windows.
func tempDir() (r string, err error) {
	prefix := fmt.Sprintf("test-%s-%v-*", filepath.Base(os.Args[0]), time.Now().UnixNano())
	return os.MkdirTemp("", prefix)
}

func TestKV(t *testing.T) {
	dir, err := tempDir()
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(dir)

	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	if err := os.Chdir(dir); err != nil {
		t.Fatal(err)
	}

	a, err := newApp()
	if err != nil {
		t.Fatal(err)
	}

	kv := a.kv
	var value any
	if err = kv.Get(nil, "/foo/50", &value); err == nil || err != sql.ErrNoRows {
		t.Fatalf("expected sql.ErrNoRows: got %T(%[1]v)", err)
	}

	pairs, err := kv.GetLike(nil, "/foo/%")
	if err != nil {
		t.Fatalf("err: %v", err)
	}

	if len(pairs) != 0 {
		t.Fatalf("expected no pairs: got %d", len(pairs))
	}

	if err = kv.Set(nil, "/foo/50", nil); err != nil {
		t.Fatal(err)
	}

	var nullInt64 sql.NullInt64
	if err = kv.Get(nil, "/foo/50", &nullInt64); err != nil {
		t.Fatal(err)
	}

	if nullInt64.Valid {
		t.Fatal("expected NULL")
	}

	if err = kv.Set(nil, "/foo/50", 500); err != nil {
		t.Fatal(err)
	}

	if err = kv.Get(nil, "/foo/50", &nullInt64); err != nil {
		t.Fatal(err)
	}

	if !nullInt64.Valid {
		t.Fatal("unexpected NULL")
	}

	if nullInt64.Int64 != 500 {
		t.Fatalf("unexpected value %v", nullInt64.Int64)
	}

	if err = kv.BatchSet(nil, []KVPair{
		{"/foo/10", 1000},
		{"/foo/20", 2000},
		{"/foo/30", 3000},
		{"/foo/40", 4000},
		{"/foo/50", 5000},
		{"/foo/60", 6000},
		{"/foo/70", 7000},
	}); err != nil {
		t.Fatal(err)
	}

	if pairs, err = kv.GetLike(nil, "/foo/%"); err != nil {
		t.Fatal(err)
	}

	verifyPairs(t, pairs, []KVPair{
		{"/foo/10", int64(1000)},
		{"/foo/20", int64(2000)},
		{"/foo/30", int64(3000)},
		{"/foo/40", int64(4000)},
		{"/foo/50", int64(5000)},
		{"/foo/60", int64(6000)},
		{"/foo/70", int64(7000)},
	})

	if err = kv.Delete(nil, "/foo/40"); err != nil {
		t.Fatal(err)
	}

	if pairs, err = kv.GetLike(nil, "/foo/%"); err != nil {
		t.Fatal(err)
	}

	verifyPairs(t, pairs, []KVPair{
		{"/foo/10", int64(1000)},
		{"/foo/20", int64(2000)},
		{"/foo/30", int64(3000)},
		{"/foo/50", int64(5000)},
		{"/foo/60", int64(6000)},
		{"/foo/70", int64(7000)},
	})

	if err := kv.BatchSet(nil, []KVPair{
		{"/foo/40", int64(3)},
		{"/foo/40/a", "aaa"},
		{"/foo/40/b", "bbb"},
		{"/foo/40/c", "ccc"},
	}); err != nil {
		t.Fatal(err)
	}

	if pairs, err = kv.GetLike(nil, "/foo/%"); err != nil {
		t.Fatal(err)
	}

	verifyPairs(t, pairs, []KVPair{
		{"/foo/10", int64(1000)},
		{"/foo/20", int64(2000)},
		{"/foo/30", int64(3000)},
		{"/foo/40", int64(3)},
		{"/foo/40/a", "aaa"},
		{"/foo/40/b", "bbb"},
		{"/foo/40/c", "ccc"},
		{"/foo/50", int64(5000)},
		{"/foo/60", int64(6000)},
		{"/foo/70", int64(7000)},
	})

	if err = kv.DeleteLike(nil, "/foo/40/%"); err != nil {
		t.Fatal(err)
	}

	if pairs, err = kv.GetLike(nil, "/foo/%"); err != nil {
		t.Fatal(err)
	}

	verifyPairs(t, pairs, []KVPair{
		{"/foo/10", int64(1000)},
		{"/foo/20", int64(2000)},
		{"/foo/30", int64(3000)},
		{"/foo/40", int64(3)},
		{"/foo/50", int64(5000)},
		{"/foo/60", int64(6000)},
		{"/foo/70", int64(7000)},
	})
}

func verifyPairs(t *testing.T, got, exp []KVPair) {
	if g, e := len(got), len(exp); g != e {
		t.Fatalf("got %v pairs, expected %v", g, e)
	}

	for i, v := range got {
		exp := exp[i]
		if g, e := v.Key, exp.Key; g != e {
			t.Fatalf("pair[%d]: got key %v, expected %v", i, g, e)
		}
		if g, e := v.Value, exp.Value; g != e {
			t.Fatalf("pair[%d]: got value %v, expected %v", i, g, e)
		}
	}
}
