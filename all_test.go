// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the GO-LICENSE file.

package main // import "modernc.org/dyd"

import (
	"encoding/hex"
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/pmezard/go-difflib/difflib"
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

type testTok struct {
	ch       rune
	sep, src string
}

func TestPreprocess(t *testing.T) {
	for i, test := range []struct {
		in        string
		toks      []testTok
		wantError bool
	}{
		{toks: []testTok{{ch: tokEOF}}},
		{" a ", []testTok{
			{ch: tokHTML, src: " a "},
			{ch: tokEOF},
		}, false},
		{"a<", []testTok{
			{ch: tokHTML, src: "a<"},
			{ch: tokEOF},
		}, false},
		{"a<b", []testTok{
			{ch: tokHTML, src: "a<b"},
			{ch: tokEOF},
		}, false},
		{"<b", []testTok{
			{ch: tokHTML, src: "<b"},
			{ch: tokEOF},
		}, false},
		{in: "a<?", wantError: true},
		{in: "<?", wantError: true},
		{in: "a<??", wantError: true},
		{in: "<??", wantError: true},
		{"a<??>", []testTok{
			{ch: tokHTML, src: "a"},
			{ch: tokPI, src: "<??>"},
			{ch: tokEOF},
		}, false},
		{in: "a<?? >", wantError: true},
		{in: "<?? >", wantError: true},
		{"a<? ?>", []testTok{
			{ch: tokHTML, src: "a"},
			{ch: tokPI, src: "<? ?>"},
			{ch: tokEOF},
		}, false},
		{"<? ?>", []testTok{
			{ch: tokPI, src: "<? ?>"},
			{ch: tokEOF},
		}, false},
		{"a<? ?>b", []testTok{
			{ch: tokHTML, src: "a"},
			{ch: tokPI, src: "<? ?>"},
			{ch: tokHTML, src: "b"},
			{ch: tokEOF},
		}, false},
		{"<", []testTok{
			{ch: tokHTML, src: "<"},
			{ch: tokEOF},
		}, false},
		{in: "<?<", wantError: true},
		{in: "<?<?", wantError: true},
		{"<?< ?>", []testTok{
			{ch: tokPI, src: "<?< ?>"},
			{ch: tokEOF},
		}, false},
		{"?", []testTok{
			{ch: tokHTML, src: "?"},
			{ch: tokEOF},
		}, false},
		{"?x", []testTok{
			{ch: tokHTML, src: "?x"},
			{ch: tokEOF},
		}, false},
		{in: "?>", wantError: true},
	} {
		toks, err := tokenizeHTML(fmt.Sprintf("%d.html", i), []byte(test.in))
		if g, e := err != nil, test.wantError; g != e {
			t.Fatalf("%d: err %v, wantError %v", i, err, e)
		}

		if err != nil {
			continue
		}

		g := toks
		e := test.toks
		if len(g) != len(e) {
			t.Fatalf("%d: got %v tokens, expected %v", i, len(g), len(e))
		}

		for j, gtok := range g {
			etok := e[j]
			if g, e := gtok.Ch, etok.ch; g != e {
				t.Fatalf("%d.%d: Ch got %#U, exp %#U", i, j, g, e)
			}

			if g, e := gtok.Sep(), etok.sep; g != e {
				t.Fatalf("%d.%d: Sep got %q, exp %q", i, j, g, e)
			}

			if g, e := gtok.Src(), etok.src; g != e {
				t.Fatalf("%d.%d: Src got %q, exp %q", i, j, g, e)
			}
		}
	}
}

// testing.TempDir seems to have some issues on windows.
func tempDir() (r string, err error) {
	prefix := fmt.Sprintf("test-%s-%v-*", filepath.Base(os.Args[0]), time.Now().UnixNano())
	return os.MkdirTemp("", prefix)
}

func TestFmt(t *testing.T) {
	dir, err := tempDir()
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(dir)

	for i, test := range []struct {
		in  string
		out string
	}{

		{"", ""},
		{"<html> </html>", "<html></html>"},
		{"<html> \n </html>", "<html>\n</html>"},
		{"<html> \n</html>", "<html>\n</html>"},
		{"<html></html>", "<html></html>"},

		{"<html>\n</html>", "<html>\n</html>"},
		{`<div>a <a href="foo">bar</a> z</div>`, `<div>a <a href="foo">bar</a> z</div>`},
		{`<div>a <a href="foo">bar</a>z</div>`, `<div>a <a href="foo">bar</a>z</div>`},
		{`<div>a<a href="foo">bar</a> z</div>`, `<div>a<a href="foo">bar</a> z</div>`},
		{`<div>a<a href="foo">bar</a>z</div>`, `<div>a<a href="foo">bar</a>z</div>`},

		{`a <a href="foo">bar</a> z`, `a <a href="foo">bar</a> z`},
		{`a <a href="foo">bar</a>z`, `a <a href="foo">bar</a>z`},
		{`a<a href="foo">bar</a> z`, `a<a href="foo">bar</a> z`},
		{`a<a href="foo">bar</a>z`, `a<a href="foo">bar</a>z`},
		{" \n\n \n<html></html> \n\n \n ", "<html></html>\n"},
	} {

		path := filepath.Join(dir, "index.html")
		if err := os.WriteFile(path, []byte(test.in), 0660); err != nil {
			t.Fatal(i, err)
		}

		if err := htmlFmt(path); err != nil {
			t.Fatal(i, err)
		}

		b, err := os.ReadFile(path)
		if err != nil {
			t.Fatal(i, err)
		}

		if g, e := string(b), test.out; g != e {
			diff := difflib.UnifiedDiff{
				A:        difflib.SplitLines(e),
				B:        difflib.SplitLines(g),
				FromFile: "expected",
				ToFile:   "got",
				Context:  0,
			}
			s, _ := difflib.GetUnifiedDiffString(diff)
			t.Errorf(
				"%v: diff for test.in `%s`\n%v\n--- expected\n%s\n\n--- got\n%s\n\n--- expected\n%s\n--- got\n%s",
				i, test.in, s, e, g, hex.Dump([]byte(e)), hex.Dump([]byte(g)),
			)
		}
	}
}
