package main

import (
	"embed"
	"log"
	"net/http"
)

var (
	//go:embed images
	images embed.FS
)

func main() {
	http.Handle("/images/", http.FileServer(http.FS(images)))
	log.Fatal(dydStart())
}
