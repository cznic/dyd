package main

import (
	"embed"
	"log"
	"net/http"
)

var (
	//go:embed *.css
	css embed.FS
)

func main() {
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.FS(css))))
	log.Fatal(dydStart())
}
