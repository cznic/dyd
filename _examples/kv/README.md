![0.png](0.png "0.png")

You can execute the example by running `$ dyd -serve` in this directory and
pointing your browser to http://localhost:6226.

To only convert the example into a go install-able repository issue `$ dyd`.

In either case here are the files after running dyd:

* `_examples/kv/index.html`

````html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>KV example</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style><? write("%s", ctx.W3CSS()) ?></style>
  </head>
  <body style="background-color: #5864ff;">
    <div class="w3-container w3-text-white">
      <p><?
	// A web server logging visits in a persistent key/value store and showing the
	// last visited time, if any.

	kv := ctx.KV()
	var cnt sql.NullInt64
	if err := kv.Get(nil, "/visits/count", &cnt); err != nil && err != sql.ErrNoRows {
	        write("get count: %v", err)
		return
	}

	if !cnt.Valid {
		cnt.Int64 = 0
	}
	cnt.Int64++
	write("visit count: %d", cnt.Int64)

	var t sql.NullTime
	if err := kv.GetTime(nil, "/visits/time", &t); err != nil  && err != sql.ErrNoRows {
	        write("get last visit time: %v", err)
		return
	}

	if t.Valid {
		write(", last visit at %s", t.Time.Format(time.RFC1123Z))
	}

	if err := kv.BatchSet(nil, []dyd.KVPair{{"/visits/count", cnt.Int64}, {"/visits/time", time.Now()}}); err != nil {
		write("batch set: %v", err)
	}
        ?>
      </p>
    </div>
  </body>
</html>

````

* `_examples/kv/dyd.go`

````go
// Code generated by dyd, DO NOT EDIT.

package main

import (
	"golang.org/x/net/html"
	"modernc.org/dyd/dyd"
)

// dydStart starts the webapp. dydStart always returns a non-nil error.
func dydStart(beforeListen ...func(a dyd.App) error) error {
	app, err := dyd.NewApp()
	if err != nil {
		return err
	}

	app.Bind("/index.html", Serve_index_1html)
	app.AddMeta("/index.html", []html.Attribute{
		html.Attribute{Namespace: "", Key: "charset", Val: "utf-8"},
	})
	app.AddMeta("/index.html", []html.Attribute{
		html.Attribute{Namespace: "", Key: "name", Val: "viewport"},
		html.Attribute{Namespace: "", Key: "content", Val: "width=device-width, initial-scale=1"},
	})
	app.AddTitle("/index.html", "KV example")
	for _, f := range beforeListen {
		if err = f(app); err != nil {
			return err
		}
	}
	return app.ListenAndServe(":6226")
}

````

* `_examples/kv/index.html.go`

````go
// Code generated by dyd, DO NOT EDIT.

package main

import (
	"database/sql"
	"time"

	"modernc.org/dyd/dyd"
)

// Serve_index_1html produces /index.html.
func Serve_index_1html(ctx *dyd.Context, write func(s string, args ...any) error) {
	write(`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>KV example</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>`)
	write("%s", ctx.W3CSS())
	write(`</style>
  </head>
  <body style="background-color: #5864ff;">
    <div class="w3-container w3-text-white">
      <p>`)

	// A web server logging visits in a persistent key/value store and showing the
	// last visited time, if any.

	kv := ctx.KV()
	var cnt sql.NullInt64
	if err := kv.Get(nil, "/visits/count", &cnt); err != nil && err != sql.ErrNoRows {
		write("get count: %v", err)
		return
	}

	if !cnt.Valid {
		cnt.Int64 = 0
	}
	cnt.Int64++
	write("visit count: %d", cnt.Int64)

	var t sql.NullTime
	if err := kv.GetTime(nil, "/visits/time", &t); err != nil && err != sql.ErrNoRows {
		write("get last visit time: %v", err)
		return
	}

	if t.Valid {
		write(", last visit at %s", t.Time.Format(time.RFC1123Z))
	}

	if err := kv.BatchSet(nil, []dyd.KVPair{{"/visits/count", cnt.Int64}, {"/visits/time", time.Now()}}); err != nil {
		write("batch set: %v", err)
	}

	write(`
      </p>
    </div>
  </body>
</html>
`)
}

````

* `_examples/kv/main.go`

````go
// Code generated by dyd, DO NOT EDIT.

package main

import "log"

func main() {
	log.Fatal(dydStart())
}

````

