package main

import (
	"bytes"
	"context"
	"fmt"
	"time"

	"modernc.org/dyd/dyd"
)

func message(ctx *dyd.Context) (r []byte) {
	b := bytes.NewBuffer(nil)

	conn, err := ctx.DBConn(context.Background())
	if err != nil {
		fmt.Fprintf(b, "cannot connect to database: %v", err)
		return
	}

	if _, err := conn.ExecContext(context.Background(), "create table if not exists log(visited timestamp);"); err != nil {
		fmt.Fprintf(b, "cannot create table 'log': %v", err)
		return
	}

	row := conn.QueryRowContext(context.Background(), "select count(*) from log;")
	var cnt int64
	if err := row.Scan(&cnt); err != nil {
		fmt.Fprintf(b, "scanning row: %v", err)
		return
	}

	fmt.Fprintf(b, "visit count: %d", cnt+1)

	if cnt > 0 {
		row := conn.QueryRowContext(context.Background(), "select visited from log order by visited desc limit 1;")
		var last time.Time
		if err := row.Scan(&last); err != nil {
			fmt.Fprintf(b, "scanning row: %v", err)
			return
		}

		fmt.Fprintf(b, ", last visit at %s", last.Format(time.RFC1123Z))
	}

	conn.ExecContext(context.Background(), "insert into log(visited) values(?)", time.Now())
	return b.Bytes()
}
