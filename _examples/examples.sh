for dir in $(ls -d _examples/*/)
do
	dyd -htmlfmt $dir
	dst="$dir"README.md
	echo -n > $dst
	for f in $(ls $dir/*.png | sort)
	do
		b=$(basename $f)
		echo $b
		echo \!\[$b\]\($b \"$b\"\) >> $dst
		echo >> $dst
	done
	echo \
"You can execute the example by running \`$ dyd -serve\` in this directory and
pointing your browser to http://localhost:6226.

To only convert the example into a go install-able repository issue \`$ dyd\`.

In either case here are the files after running dyd:" \
	>> $dst
	echo >> $dst
	for f in $(find $dir -name \*.html | sort)
	do
		echo "* \`$f\`" >> $dst
		echo >> $dst
		echo '````html' >> $dst
		cat $f >> $dst
		echo >> $dst
		echo '````' >> $dst
		echo >> $dst
	done
	for f in $(find $dir -name \*.go | sort)
	do
		echo "* \`$f\`" >> $dst
		echo >> $dst
		echo '````go' >> $dst
		cat $f >> $dst
		echo >> $dst
		echo '````' >> $dst
		echo >> $dst
	done
	dyd -clean $dir
done
