// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/dyd"

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

// origin returns caller's short position, skipping skip frames.
func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

// todo prints and returns caller's position and an optional message tagged with TODO. Output goes to stderr.
//
//lint:ignore U1000 whatever
func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", s)
	// os.Stdout.Sync()
	return s
}

// trc prints and returns caller's position and an optional message tagged with TRC. Output goes to stderr.
//
//lint:ignore U1000 whatever
func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", s)
	os.Stderr.Sync()
	return s
}

func niceHTMLLiteral(s string) string {
	if !strings.ContainsAny(s, "\n\r") {
		return strconv.Quote(s)
	}

	a := strings.Split(s, "`")
	return "`" + strings.Join(a, "`+\n`") + "`"
}

func findGoMod(dir string) (string, error) {
	dir, err := filepath.Abs(dir)
	if err != nil {
		return "", err
	}

	dir0 := dir
	for {
		dir = filepath.Clean(dir)
		fi, err := os.Stat(dir)
		if err != nil {
			return "", err
		}

		if !fi.IsDir() {
			return "", fmt.Errorf("not a directory: %s", dir)
		}

		mod := filepath.Join(dir, "go.mod")
		if fi, err = os.Stat(mod); err == nil {
			if fi.Mode().IsRegular() {
				return mod, nil
			}

			return "", fmt.Errorf("%s: not a regular file", mod)
		}

		if !os.IsNotExist(err) {
			return "", err
		}

		dir2, _ := filepath.Split(dir)
		if dir2 == "" || dir2 == dir {
			return "", fmt.Errorf("%s is outside a module", dir0)
		}

		dir = dir2
	}
}

func encodeFileName(s string) string {
	s = strings.ReplaceAll(s, "_", "_0")
	s = strings.ReplaceAll(s, ".", "_1")
	return strings.ReplaceAll(s, "-", "_2")
}

func decodeFileName(s string) string {
	s = strings.ReplaceAll(s, "_2", "-")
	s = strings.ReplaceAll(s, "_1", ".")
	return strings.ReplaceAll(s, "_0", "_")
}

func decodeHexString(s string) string {
	var b strings.Builder
	for i := 0; i < len(s); i += 2 {
		b.WriteByte(decodeHexByte(s[i : i+2]))
	}
	return b.String()
}

func decodeHexByte(s string) byte {
	n, err := strconv.ParseUint(s, 16, 8)
	if err != nil {
		panic(fmt.Errorf("internal error: decodeHexByte(%q): %v (%v:)", s, err, origin(1)))
	}

	return byte(n)
}
