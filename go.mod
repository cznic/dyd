module modernc.org/dyd

go 1.21

require (
	github.com/golang/glog v1.2.3
	github.com/pmezard/go-difflib v1.0.0
	golang.org/x/mod v0.17.0 // *
	golang.org/x/net v0.33.0
	modernc.org/gc/v3 v3.0.0-20240304020402-f0dba7c97c2b // *
	modernc.org/opt v0.1.3
	modernc.org/scanner v1.2.0
	modernc.org/sqlite v1.34.4
	modernc.org/strutil v1.2.0
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/sys v0.28.0 // indirect
	modernc.org/libc v1.55.3 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
