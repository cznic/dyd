// Copyright 2023 The dyd Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/dyd"

import (
	"modernc.org/scanner"
)

const (
	tokEOF = iota
	tokHTML
	tokPI
)

func tokenizeHTML(name string, b []byte) (r []scanner.Token, err error) {
	var off int
	var s *scanner.Scanner
	errBudget := 10
	s = scanner.NewScanner(
		name,
		b,
		func() int { return 0 },
		func() (length int, ch rune) {
			if off >= len(b) || errBudget <= 0 {
				return 0, tokEOF
			}

			off0 := off
			switch b[off] {
			case '<':
				if off+1 == len(b) {
					break
				}

				switch b[off+1] {
				case '?':
					// scan processing instruction
					off += 2 // "<?"
					for {
						if off == len(b) {
							s.AddErr(s.Position(off0), "processing instruction tag not closed")
							return off - off0, tokPI
						}

						switch b[off] {
						case '\n':
							off++
							s.AddLine(off)
						case '<':
							off++
							if off == len(b) {
								s.AddErr(s.Position(off0), "processing instruction tag not closed")
								return off - off0, tokPI
							}

							switch b[off] {
							case '?':
								s.AddErr(s.Position(off-1), "nested processing instruction tags not allowed")
								off++
								errBudget--
							default:
								off++
							}
						case '?':
							off++
							if off == len(b) {
								s.AddErr(s.Position(off0), "processing instruction tag not closed")
								return off - off0, tokPI
							}

							switch b[off] {
							case '>':
								off++
								return off - off0, tokPI
							}

							off++
						default:
							off++
						}
					}
				}
			}

			// scan HTML
			for {
				if off == len(b) || errBudget <= 0 {
					return off - off0, tokHTML
				}

				switch b[off] {
				case '\n':
					off++
					s.AddLine(off)
				case '<':
					if off+1 == len(b) {
						off++
						return off - off0, tokHTML
					}

					switch b[off+1] {
					case '?':
						return off - off0, tokHTML
					default:
						off++
					}
				case '?':
					if off+1 == len(b) {
						off++
						return off - off0, tokHTML
					}

					switch b[off+1] {
					case '>':
						s.AddErr(s.Position(off), "unexpected processing instruction closing tag")
						off++
						errBudget--
					default:
						off += 2
					}
				default:
					off++
				}
			}
		},
	)

	for {
		tok := s.Scan()
		r = append(r, tok)
		if tok.Ch == tokEOF {
			return r, s.Err()
		}
	}
}
